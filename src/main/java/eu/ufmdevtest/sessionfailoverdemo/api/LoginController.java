package eu.ufmdevtest.sessionfailoverdemo.api;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

@RestController
public class LoginController {
    @Autowired
    HttpSession session;

    @GetMapping("/")
    public String getHello(@RequestParam(required = false) String username) {
        Person person = (Person) session.getAttribute("person");
        if (person == null) {
            person = new Person();
            person.setName(username);
            person.setMotto("A random motto is better than no motto!");
            session.setAttribute("person", person);
        }
        return "\nHello '" + username + "' - I know YOU! Your motto is '" + person.getMotto() + "'\n";
    }

    @PostMapping("/")
    public void updateMotto(@RequestPart String motto) {
        Person person = (Person) session.getAttribute("person");
        if (person != null) {
            person.setMotto(motto);
            session.setAttribute("person", person);
        }
    }

}
