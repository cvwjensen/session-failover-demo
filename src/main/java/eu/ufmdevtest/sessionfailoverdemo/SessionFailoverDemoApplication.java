package eu.ufmdevtest.sessionfailoverdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan(basePackages = {"eu.ufmdevtest"})
public class SessionFailoverDemoApplication {

    public static void main(String[] args) {
        ConfigurableApplicationContext run = SpringApplication.run(SessionFailoverDemoApplication.class, args);
        Object redisTemplate = run.getBean("redisTemplate");
    }

}
